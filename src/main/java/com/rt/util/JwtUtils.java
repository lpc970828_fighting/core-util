package com.rt.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.rt.constants.Code;
import com.rt.constants.Constants;
import com.rt.constants.Message;
import com.rt.exception.CustomException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class JwtUtils {

    // 签名有效时间(s)
    private static final Long ttl = 600L;

    /**
     * 生成签名
     *
     * @param userId 用户ssoId
     * @param secret 用户密码，使用Algorithm.HMAC256(secret)作为签发token的秘钥。若签发token时都使用同一个秘钥，会存在秘钥泄露的风险。
     */
    public static String createToken(String userId, String secret) {
        try {
            Date date = new Date(System.currentTimeMillis() + ttl * 1000);
            // 附带username信息
            return JWT.create()
                    .withClaim("userId", userId)
                    .withExpiresAt(date)
                    .sign(Algorithm.HMAC256(secret));
        } catch (UnsupportedEncodingException e) {
            log.error("JwtUtils#createToken 生成token失败： {}", e.getMessage());
            throw new CustomException(Code.SYSTEM_ERROR.value(), e.getMessage());
        }
    }


    /**
     * @param userId:
     * @param secret:
     * @param tokenExpiresTime: token失效时间 单位秒
     * @param userType:         用户类型：user/client  非密码模式全是client入值
     * @param permission:       用户类型为user的权限 client不存入
     * @param grantType:        哪种授权模式 authorization_code/password/client_credentials/refresh_token
     * @Description
     * @Date 10:51 2020/7/3
     **/
    public static String createAccessToken(String userId, String secret,
                                           Long tokenExpiresTime, String userType,
                                           List<String> permission, String grantType,
                                           String clientId, String uuId) {
        try {
            Date date = new Date(System.currentTimeMillis() + tokenExpiresTime * 1000);
            // 附带username信息
            return JWT.create()
                    /*
                     * 令牌的唯一标识符。该声明的值在令牌颁发者创建的每一个令牌中都是唯一的，为了防止冲突，它通常
                     * 是一个密码学随机值。这个值相当于向结构化令牌中加入了一个攻击者无法获得的随机熵组件，有利于防止令牌猜测攻击和重放攻击
                     * 可以应用于缓存中的 key
                     */
                    .withJWTId(StringUtils.uuid())
                    /*
                     * 令牌的主题。它表示该令牌是关于谁的，在很多 OAuth 部署中会将它设为资源拥有者的唯一标识。在
                     * 大多数情况下，主体在同一个颁发者的范围内必须是唯一的。该声明是一个字符串
                     */
                    .withSubject(userId)
                    // 令牌颁发者。它表示该令牌是由谁创建的，在很多 OAuth 部署中会将它设为授权服务器的 URL。该声明是一个字符串
                    .withIssuer(Constants.WITH_ISSSUSER)
                    // 令牌接收者
                    .withAudience(userId)
                    // 令牌颁发时间
                    .withIssuedAt(new Date())
                    // .claim(name,value) 可以设置一些业务的属性
                    .withClaim(Constants.USERID, userId)
                    .withClaim(Constants.USERTYPE, userType)
                    .withClaim(Constants.PERMISSION, permission.toString())
                    .withClaim(Constants.GRANT_TYPE, grantType)
                    .withClaim(Constants.CLIENT_ID, clientId)
                    .withClaim(Constants.UUID, uuId)
                    //签名过期时间
                    .withExpiresAt(date)
                    //// 签名密钥, 必须
                    .sign(Algorithm.HMAC256(secret));
        } catch (UnsupportedEncodingException e) {
            log.error("JwtUtils#createToken 生成token失败： {}", e.getMessage());
            throw new CustomException(Code.SYSTEM_ERROR.value(), e.getMessage());
        }
    }

    /**
     * @param userId:
     * @param secret:
     * @param tokenExpiresTime: //     * @param permission:
     * @Description 创建refreshToken
     * @Date 23:29 2020/6/27
     **/
    public static String createRefreshToken(String userId, String secret,
                                            Long tokenExpiresTime, String userType,
                                            List<String> permission, String grantType,
                                            String clientId, String uuId) {
        try {
            Date date = new Date(System.currentTimeMillis() + tokenExpiresTime * 1000);
            // 附带username信息
            return JWT.create()
                    /*
                     * 令牌的唯一标识符。该声明的值在令牌颁发者创建的每一个令牌中都是唯一的，为了防止冲突，它通常
                     * 是一个密码学随机值。这个值相当于向结构化令牌中加入了一个攻击者无法获得的随机熵组件，有利于防止令牌猜测攻击和重放攻击
                     * 可以应用于缓存中的 key
                     */
                    .withJWTId(StringUtils.uuid())
                    /*
                     * 令牌的主题。它表示该令牌是关于谁的，在很多 OAuth 部署中会将它设为资源拥有者的唯一标识。在
                     * 大多数情况下，主体在同一个颁发者的范围内必须是唯一的。该声明是一个字符串
                     */
                    .withSubject(userId)
                    // 令牌颁发者。它表示该令牌是由谁创建的，在很多 OAuth 部署中会将它设为授权服务器的 URL。该声明是一个字符串
                    .withIssuer(Constants.WITH_ISSSUSER)
                    // 令牌接收者
                    .withAudience(userId)
                    // 令牌颁发时间
                    .withIssuedAt(new Date())
                    // .claim(name,value) 可以设置一些业务的属性
                    .withClaim(Constants.USERID, userId)
                    .withClaim(Constants.USERTYPE, userType)
                    .withClaim(Constants.PERMISSION, permission.toString())
                    .withClaim(Constants.GRANT_TYPE, grantType)
                    .withClaim(Constants.CLIENT_ID, clientId)
                    .withClaim(Constants.UUID, uuId)
                    //签名过期时间
                    .withExpiresAt(date)
                    //// 签名密钥, 必须
                    .sign(Algorithm.HMAC512(secret));
        } catch (UnsupportedEncodingException e) {
            log.error("JwtUtils#createToken 生成token失败： {}", e.getMessage());
            throw new CustomException(Code.SYSTEM_ERROR.value(), e.getMessage());
        }
    }

    /**
     * 解析accessToken
     *
     * @param token  token
     * @param userId 用户ssoId
     * @param secret 用户密码
     */
    public static boolean parseToken(String token, String userId, String secret) {
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(secret))
                    .withClaim("userId", userId)
                    .build();
            verifier.verify(token);
            return true;
        } catch (Exception e) {
            log.error("JwtUtils#parseToken 解析token失败： {}", e.getMessage());
            throw new CustomException(Code.SYSTEM_ERROR.value(), e.getMessage());
        }
    }

    /**
     * @param token:
     * @param userId:
     * @param secret:
     * @Description 解析refreshToken
     * @Date 23:27 2020/6/27
     **/
    public static boolean parseRefreshToken(String token, String userId, String secret) {
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC512(secret))
                    .withClaim("userId", userId)
                    .build();
            verifier.verify(token);
            return true;
        } catch (Exception e) {
            log.error("JwtUtils#parseToken 解析token失败： {}", e.getMessage());
            throw new CustomException(Code.SYSTEM_ERROR.value(), e.getMessage());
        }
    }

    // 获取用户名
    public static String getSsoId(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("userId").asString();
        } catch (JWTDecodeException e) {
            log.error("JwtUtils#getSsoId 获取ssoId失败： {}", e.getMessage());
            return null;
        }
    }

    // 获取Claims
    public static Map<String, Claim> getClaims(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaims();
        } catch (Exception e) {
            log.error("JwtUtils#getClaims 获取claims失败： {}", e.getMessage());
            throw new CustomException(Code.TOKEN_INVALID.value(), Message.TOKEN_INVALID.value());
        }
    }

    // token是否过期
    public static boolean isExpired(String token) {
        Date now = Calendar.getInstance().getTime();
        DecodedJWT jwt = JWT.decode(token);
        return jwt.getExpiresAt().before(now);
    }


}
