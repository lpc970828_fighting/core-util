package com.rt.util;


import com.rt.constants.Code;
import com.rt.exceptions.ParameterFormatOrTypeValidationException;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author：Ray
 * @ProjectName：Information
 * @Description：工具屋
 * @date：14:47 2020/3/5
 */
public class HovelUtil {

    public HovelUtil() {
    }

    /**
     * 对象属性转义
     *
     * @param object
     * @return
     */
    public static Object sqlEscape(Object object) {
        try {
            Map<Object, Object> map = new HashMap<>();
            Class<?> aClass = object.getClass();
            Field[] fields = aClass.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                if ("class java.lang.String".equals(field.getGenericType().toString())) {
                    Method method = (Method) object.getClass().getMethod("get" + getMethodName(field.getName()));
                    // 拼接属性名
                    String name = method.getName();
                    name = name.substring(name.indexOf("get") + 3);
                    name = name.toLowerCase().charAt(0) + name.substring(1);
                    // 执行方法
                    String invoke = (String) method.invoke(object, (Object[]) null);
                    String result = StringUtils.isBlank(invoke) ? null : invoke.replaceAll("\\\\", "\\\\\\\\").replaceAll("%", "\\\\%").replaceAll("_", "\\\\_");
                    map.put(name, result);
                    continue;
                }
                Method method = (Method) object.getClass().getMethod("get" + getMethodName(field.getName()));
                // 拼接属性名
                String name = method.getName();
                name = name.substring(name.indexOf("get") + 3);
                name = name.toLowerCase().charAt(0) + name.substring(1);
                Object result = field.get(object);
                map.put(name, result);
            }
            return HovelUtil.map2JavaBean(aClass, map);
        } catch (Exception e) {
            return "map转对象的时候的错误" + e;
        }
    }

    public static Object map2JavaBean(Class<?> clazz, Map<Object, Object> map) throws Exception {
        // 构建对象
        Object javabean = clazz.newInstance();
        // 获取所有方法
        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            if (method.getName().startsWith("set")) {
                // 截取属性名
                String field = method.getName();
                field = field.substring(field.indexOf("set") + 3);
                field = field.toLowerCase().charAt(0) + field.substring(1);
                if (map.containsKey(field)) {
                    method.invoke(javabean, map.get(field));
                }
            }
        }
        return javabean;
    }

    private static String getMethodName(String fildeName) throws Exception {
        byte[] items = fildeName.getBytes();
        items[0] = (byte) ((char) items[0] - 'a' + 'A');
        return new String(items);
    }

    /**
     * 正则验证电话号码的正确性
     */
    public static void verifyMobile(String mobile) {

        String regex = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[013678])|(18[0,5-9]))\\d{8}$";
        if (null == mobile || !Pattern.compile(regex).matcher(mobile).matches()) {
            throw new ParameterFormatOrTypeValidationException(Code.FAIL.value(), "手机号码格式不正确！");
        }
    }



}