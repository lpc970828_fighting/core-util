package com.rt.util;

import com.rt.constants.Code;
import com.rt.exception.CustomException;
import com.vdurmont.emoji.EmojiManager;
import lombok.extern.slf4j.Slf4j;

import javax.management.relation.Role;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class StringUtils extends org.apache.commons.lang3.StringUtils {


    /**
     * @Description 生成5位随机大小写字母和数字组成的验证码 用于授权码模式中生成code
     * @Date 16:57 2020/6/23
     **/
    public String verifyCode() {
        String str = "";
        char[] ch = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
                'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            char num = ch[random.nextInt(ch.length)];
            str += num;
        }
        return str;

    }

    /**
     * 判断入参是否是邮箱
     */
    public static Boolean isEmail(String str) {
        /*
         * " \w"：匹配字母、数字、下划线。等价于'[A-Za-z0-9_]'。
         * "|"  : 或的意思，就是二选一
         * "*" : 出现0次或者多次
         * "+" : 出现1次或者多次
         * "{n,m}" : 至少出现n个，最多出现m个
         * "$" : 以前面的字符结束
         */
        String regex = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
        Pattern p = Pattern.compile(regex);
        Matcher matcher = p.matcher(str);
        return matcher.matches();
    }

    /**
     * @param args:
     * @Description 字符判空 拓展StringUtils.isBlank
     * @Date 11:25 2020/7/22
     **/
    public static boolean isNullStr(String... args) {
        boolean falg = false;
        for (String arg : args) {
            if (StringUtils.isBlank(arg) || "null".equals(arg)) {
                falg = true;
                return falg;
            }
        }
        return falg;
    }

    /**
     * 判断入参是否是手机号
     */
    public static Boolean isMobilePhone(String str) {
        String regex = "^[1]([3-9])[0-9]{9}$";
        Pattern p = Pattern.compile(regex);
        Matcher matcher = p.matcher(filterMobilePhoneNum(str));
        return matcher.matches();
    }

    /**
     * 判断入参是否是ssoId
     */
    public static Boolean isSsoId(String str) {
        return str.startsWith("ZY") && str.length() == 14;
    }

    /**
     * @Description 去掉-的uuid
     * @Date 16:22 2020/6/29
     **/
    public static String uuid() {
        String s = UUID.randomUUID().toString();
        return s.substring(0, 8) + s.substring(9, 13) + s.substring(14, 18) + s.substring(19, 23) + s.substring(24);
    }

    /**
     * @param str:
     * @Description 判断字符串是否含有表情符
     * @Date 18:55 2020/6/29
     **/
    public static Boolean isEmoji(String str) {
        boolean falg = false;
        if (EmojiManager.containsEmoji(str)) {
            falg = true;
        }
        return falg;
    }

    /**
     * @param str:
     * @Description 表情符异常判断
     * @Date 13:56 2020/7/2
     **/
    public static void paramIsEmoji(String str) {
        if (EmojiManager.containsEmoji(str)) {
            throw new CustomException(Code.PARAMETER_VERIFY_FAILED.value(), "入参不能含有表情符！");
        }
    }


    /**
     * 过滤手机号。复制155 3040 5405到字段中，自动保存为15530405405，而不是155 3040 54
     */
    public static String filterMobilePhoneNum(String mobilePhoneNum) {
        if (mobilePhoneNum == null || mobilePhoneNum.trim().length() == 0) {
            return null;
        }
        mobilePhoneNum = mobilePhoneNum.trim();
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < mobilePhoneNum.length(); i++) {
            char c = mobilePhoneNum.charAt(i);
            if (c != ' ') {
                res.append(c);
            }
        }
        return res.toString();
    }

    ///**
    // * @param permissions:
    // * @Description 用户权限拆分拼接
    // * @Date 11:00 2020/7/2
    // **/
    //public static List<String> stringPermission(List<Permission> permissions) {
    //    List<String> stringList = new ArrayList<>();
    //    if (permissions != null && permissions.size() > 0) {
    //        for (Permission permission : permissions) {
    //            String str = permission.getPermSign();
    //            stringList.add(str);
    //        }
    //    }
    //    return stringList;
    //}

    /**
     * @param roles:
     * @Description 角色名拼接
     * @Date 11:01 2020/7/2
     **/
    public static List<String> stringRole(List<Role> roles) {
        List<String> stringList = new ArrayList<>();
        for (Role role : roles) {
            String str = role.getRoleName();
            stringList.add(str);
        }

        return stringList;
    }

    ///***
    // *  分页公共方法
    // *  PS：级联查询的语句部分会出现500异常（两张表中有“相同字段名”时会出现500异常）
    // * @date 2020/7/13 13:53
    // * @return {@link }
    // * @throws
    // **/
    //public static void configPageHelper(QueryModel queryModel, Class clazz) {
    //    Integer pageNum = queryModel.getPageNum();
    //    Integer pageSize = queryModel.getPageSize();
    //    String orderBy = queryModel.getOrderBy();
    //    Integer assending = queryModel.getAssending();
    //    if (assending != null && StringUtils.isEmpty(orderBy)) {
    //        throw new BaseException(Code.BUSINESS_EXCEPTIONS.value(), "排序方式【assending]不为空时入参【orderBy】不能为空！");
    //    }
    //    if (StringUtils.isNotEmpty(orderBy) && assending != null) {
    //        Class<?> aClass = null;
    //        try {
    //            aClass = Class.forName(clazz.getName());
    //            Field[] fields = aClass.getDeclaredFields();
    //
    //            List<String> fieldNameList = new ArrayList<>();
    //            for (Field filed : fields) {
    //                fieldNameList.add(filed.getName());
    //            }
    //            if (!fieldNameList.contains(orderBy)) {
    //                throw new BaseException(Code.PARAM_VALIDATE_FAILED.value(), "排序字段输入有误！");
    //            }
    //        } catch (ClassNotFoundException e) {
    //            log.error(e.toString(), e);
    //        }
    //        String convertOrderBy = CamelUnderLineUtil.camel2Underline(orderBy);
    //        String convertAssending = QueryAssending.getById(assending).getValue();
    //        PageHelper.startPage(pageNum, pageSize, convertOrderBy + " " + convertAssending);
    //    } else {
    //        PageHelper.startPage(pageNum, pageSize);
    //    }
    //}
    //
    ///**
    // * @param content:
    // * @Description 判断字符串是否是json 对象集合
    // * @Date 19:00 2020/7/10
    // **/
    //public static boolean isJson(String content) {
    //    try {
    //        JSONArray jsonArray = JSONArray.fromObject(content);
    //        return true;
    //    } catch (Exception e) {
    //        return false;
    //    }
    //}
    //
    //
    ///**
    // * 获取 root tree, parentId == -1
    // *
    // * @param treeList tree list
    // * @return root tree
    // */
    //private static List<Hierarchy> getRootTree(List<Hierarchy> treeList) {
    //    return treeList.stream()
    //            .filter(t -> t.getParentId() == -1)
    //            .collect(Collectors.toList());
    //}

//    /**
//     * list to tree
//     *
//     * @param treeList tree list
//     * @return root tree
//     */
//    public static List<Hierarchy> convertToTree(List<Hierarchy> treeList) {
//        // get tree map
//        Map<Integer, Hierarchy> treeMap = treeList.stream()
//                .collect(Collectors.toMap(
//                        Hierarchy::getId, t -> t
//                ));
//
//        for (Map.Entry<Integer, Hierarchy> treeEntry : treeMap.entrySet()) {
//            Hierarchy t = treeEntry.getValue();
//            Hierarchy parentTree = treeMap.get(t.getParentId());
//            if (parentTree == null) {
//                continue;
//            }
//
//            // set parent tree
////            t.setParent(parentTree);
//
//            // add child tree
//            if (parentTree.getChildren() == null) {
//                parentTree.setChildren(Lists.newArrayList());
//            }
//            parentTree.getChildren().add(t);
//        }
//
//        return getRootTree(treeList);
//    }
//
//    public static List<Hierarchy> convertToChildTree(List<Hierarchy> treeList) {
//        // get tree map
//        Map<Integer, Hierarchy> treeMap = treeList.stream()
//                .collect(Collectors.toMap(
//                        Hierarchy::getId, t -> t
//                ));
//
//        for (Map.Entry<Integer, Hierarchy> treeEntry : treeMap.entrySet()) {
//            Hierarchy t = treeEntry.getValue();
//            Hierarchy parentTree = treeMap.get(t.getParentId());
//            if (parentTree == null) {
//                continue;
//            }
//
//            // set parent tree
////            t.setParent(parentTree);
//
//            // add child tree
//            if (parentTree.getChildren() == null) {
//                parentTree.setChildren(Lists.newArrayList());
//            }
//            parentTree.getChildren().add(t);
//        }
//
//        return getRootTree(treeList);
//    }
//
//    public static Hierarchy getParent(List<Hierarchy> hierarchies, Hierarchy hierarchy) {
//
//        List<Hierarchy> hierarchies1 = hierarchies.stream()
//                .filter(obj -> obj.getId().equals(hierarchy.getId()))
//                .collect(Collectors.toList());
//
//        Hierarchy hierarchy1 = hierarchies1.size() > 0 ? hierarchies1.get(0) : null;
//        if (null == hierarchy1 || -1 == hierarchy1.getParentId()) {
//            return hierarchy;
//        }
//
//        List<Hierarchy> hierarchies2 = hierarchies.stream()
//                .filter(obj -> obj.getId().equals(hierarchy1.getParentId()))
//                .collect(Collectors.toList());
//        hierarchy.setParent(getParent(hierarchies, hierarchies2.get(0)));
//        return hierarchy;
//    }
//
//
//    /**
//     * 转换二进制数据
//     *
//     * @return
//     * @Param [umeConfirmed, umeConfirmedEnum, operater]
//     * @Date 2020/7/17 10:59
//     **/
//    public static Integer convertUmeConfirmed(Integer umeConfirmed, UmeConfirmedEnum umeConfirmedEnum, boolean operater) {
//        umeConfirmed = umeConfirmed == null ? 0 : umeConfirmed;// umeConfirmed为空时初始化为0 ，避免空指针
//        Integer index = UmeConfirmedEnum.getIndex(umeConfirmedEnum);
//
//        if (operater && (umeConfirmed & index) == 0) {
//            umeConfirmed = umeConfirmed + index;
//        } else if (!operater && (umeConfirmed & index) > 0) {
//            umeConfirmed = umeConfirmed - index;
//        }
//        return umeConfirmed;
//    }

    ///**
    // * 过滤用户名、手机号码、邮箱是否显示
    // *
    // * @return {@link}
    // * @throws
    // * @date 2020/7/16 17:57
    // **/
    //public static void convertUsersThings(UsersThings usersThings) {
    //    Integer umeConfirmed = usersThings.getUmeConfirmed();
    //
    //    if ((umeConfirmed & UmeConfirmedEnum.getIndex(UmeConfirmedEnum.USER_NAME)) == 0) {
    //        usersThings.setUserName("");
    //    }
    //
    //    if ((umeConfirmed & UmeConfirmedEnum.getIndex(UmeConfirmedEnum.MOBILE)) == 0) {
    //        usersThings.setMobilePhoneNum("");
    //    }
    //
    //    if ((umeConfirmed & UmeConfirmedEnum.getIndex(UmeConfirmedEnum.EMIAL)) == 0) {
    //        usersThings.setEmail("");
    //    }
    //}

    public static List<String> filterApiList() {
        return Arrays.asList("/customer/**","/oauth2/token","/oauth2/refreshAccessToken");
    }

    ///**
    // * @Description 根据数值显示作用范围名称
    // * @Date 2020/7/27 10:29
    // * @Param [appInfoDetailsVo]
    // */
    //public static String getAppInfoScope(AppInfoDetailsVo appInfoDetailsVo) {
    //    StringBuilder builder = new StringBuilder();
    //    String appInfoScope = null;
    //    Integer scope = appInfoDetailsVo.getScope();
    //    if (scope != null && scope != 0) {
    //        if ((scope & AppInfoScopeEnum.getIndex(AppInfoScopeEnum.WEB)) == (AppInfoScopeEnum.WEB.value())) {
    //            builder.append(AppInfoScopeEnum.WEB.type() + "、");
    //        }
    //        if ((scope & AppInfoScopeEnum.getIndex(AppInfoScopeEnum.IOS)) == (AppInfoScopeEnum.IOS.value())) {
    //            builder.append(AppInfoScopeEnum.IOS.type() + "、");
    //        }
    //        if ((scope & AppInfoScopeEnum.getIndex(AppInfoScopeEnum.ANDROID)) == (AppInfoScopeEnum.ANDROID).value()) {
    //            builder.append(AppInfoScopeEnum.ANDROID.type() + "、");
    //        }
    //        if ((scope & AppInfoScopeEnum.getIndex(AppInfoScopeEnum.APPLET)) == (AppInfoScopeEnum.APPLET).value()) {
    //            builder.append(AppInfoScopeEnum.APPLET.type() + "、");
    //        }
    //    } else {
    //        return "";
    //    }
    //    if (StringUtils.isNotBlank(builder)) {
    //        appInfoScope = builder.toString().substring(0, builder.toString().lastIndexOf("、"));
    //    }
    //    return appInfoScope;
    //}
}