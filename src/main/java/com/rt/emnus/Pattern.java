package com.rt.emnus;

/**
 * @author Ray
 * @version 1.0
 * @date 2020/10/21 14:23
 */
public enum Pattern {


    AUTHORIZATION_CODE_GRANT("authorization_code_grant", "授权码授权模式", 1),

    IMPLICIT_GRANT("implicit_grant", "隐式授权模式", 2),

    RESOURCE_OWNER_PASSWORD_CREDENTIALS_GRANT("resource_owner_password_credentials_grant", "密码模式", 3),

    CLIENT_CREDENTIALS_GRANT("client_credentials_grant", "客户端凭证模式", 4),

    REFRESH_TOKEN("refresh_token", "刷新token", 0);

    /**
     * 模式描述
     */
    private String description;
    /**
     * 模式数字类型
     */
    private Integer dateType;
    /**
     * 模式类型
     */
    private String type;

    Pattern(String type, String description, Integer dateType) {
        this.type = type;
        this.description = description;
        this.dateType = dateType;
    }
    public String type() {
        return this.type;
    }
}
