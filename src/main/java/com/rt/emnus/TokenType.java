package com.rt.emnus;
/**
 * @Description token类型 A accessToken,R refreshToken
 * @Date 11:25 2020/6/30
 **/
public enum TokenType {
    /**
     * @Description  accessToken
     * @Date 11:30 2020/6/30
     **/
    A("A","accessToken"),

    /**
     * @Description  accessToken
     * @Date 11:30 2020/6/30
     **/
    R("R","refreshToken"),

    ;

    /**
     * @Description token类型
     * @Date 11:27 2020/6/30
     * @param null:
     **/
    private String tokenType;

    /**
     * @Description 说明
     * @Date 11:27 2020/6/30
     * @param null:
     **/
    private String tokenTypeDesc;

    TokenType(String tokenType, String tokenTypeDesc) {
        this.tokenType = tokenType;
        this.tokenTypeDesc = tokenTypeDesc;
    }

    public String tokenType() {
        return this.tokenType;
    }

    public String tokenTypeDesc() {
        return this.tokenTypeDesc;
    }
}
