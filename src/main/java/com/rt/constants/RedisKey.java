package com.rt.constants;

/**
 * <p>
 * </p>
 *
 * @create 2020/10/30 17:49
 */
public class RedisKey {

    /**
     * 这个key存放校验通过的手机号码
     */
    public static final String OPEN_PHONE_NUMBER = "openPhoneNumber";

    /**
     * 这个key存放验证码
     */
    public static final String CAPTCHA = "pro:token:refreshToken : value";

    /**
     * 这个key存放短时间内发给短信的手机号码
     */
    public static final String SEND_CODE = "sendCode";

}