package com.rt.constants;

/**
 * @author：Ray
 * @ProjectName：Information
 * @Description：自定义通用参数名
 * @date：14:47 2020/3/5
 */
public class Constants {

    public static final String USER = "user";
    public static final String TOKEN = "token";
    public static final String ACCESSTOKEN = "accesstoken";
    public static final String USER_ID = "userId";
    /**
     * @Description 使用者id  可以是ssoid 也可以是appid
     **/
    public static final String USERID = "userId";
    /**
     * @Description 使用者类型  值只能为user和client
     **/
    public static final String USERTYPE = "userType";

    /**
     * @Description 权限 创建token使用
     **/
    public static final String PERMISSION = "permission";
    public static final String GRANT_TYPE = "grantType";
    public static final String CLIENT_ID = "clientId";
    public static final String UUID = "uuId";
    public static final String BEARER = "bearer";
    /**
     * @Description 令牌颁发者
     **/
    public static final String WITH_ISSSUSER = "https://rt.com/idm";
    /**
     * 当前登录用户
     */
    public static final String CUSTOMER_USER = "customerUser";
    /**
     * @Description 用户id
     **/
    public static final String SSOID = "ssoId";
    /**
     * @Description redis中accesstoken key 的前缀
     **/
    public static final String ACCESSTOKEN_REDIS_KEY_PREFIX = "pro:token:accessToken:";

    /**
     * @Description redis中refreshToken key 的前缀
     **/
    public static final String REFRESHTOKEN_REDIS_KEY_PREFIX = "pro:token:refreshToken:";
    /**
     * 默认密码
     */
    public static final String USER_DEFAULT_PASSWORD = "123456";
    /**
     * @Description redis中的token key 前缀
     * @Date 13:55 2020/7/3
     * @param null:
     **/
    public static final String PRO_TOKEN_PREFIX = "pro:token:";
}