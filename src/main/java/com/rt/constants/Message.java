package com.rt.constants;

/**
 * @author：Ray
 * @ProjectName：Information
 * @Description：返回前端message
 * @date：17:05 2020/3/5
 */
public enum Message {

    /**
     * 成功
     */
    SUCCESS("Success"),

    /**
     * 参数验证失败
     */
    PARAMS_VALIDATION_FAILED("Parameter validation failed"),
    /**
     * 账号和密码不匹配
     * 资源未找到
     */
    INVALID_ID_OR_PASSWORD("Account and password do not match！"),
    /**
     * 未知错误
     */
    UNKNOWN_ERROR("unknown mistake"),
    /**
     * 重复操作
     */
    REPEAT_OPERATION("Repeat operation"),
    /**
     * token不合法
     */
    TOKEN_INVALID("token is illegal"),
    /**
     * 身份认证失败
     */
    AUTHENTICATION_FAILED("Identity authentication failed"),
    /**
     * 参数验证失败
     */
    PARAMETER_VALIDATION("Parameter verification failed"),
    /**
     * 系统异常错误
     */
    SYSTEM_ERROR("Internal server error");
    private String value;

    private Message(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }
}
