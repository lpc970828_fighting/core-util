package com.rt.constants;

/**
 * @author：Ray
 * @ProjectName：Information
 * @Description：自定义返回错误码
 * @date：14:47 2020/3/5
 */

public enum SubCode {

    /**
     * 参数缺失
     */
    PARAMS_MISSING("C400_1"),

    /**
     * 参数非法
     */
    INVALID_PARAMS("C400_2"),

    /**
     * 权限验证失败
     */
    AUTHENTICATION_FAILED("Q401_1");

    private String value;

    SubCode(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}