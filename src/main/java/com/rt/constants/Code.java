package com.rt.constants;

/**
 * @author：Ray
 * @ProjectName：Information
 * @Description：返回前端code
 * @date：17:08 2020/3/5
 */
public enum Code {

    /**
     * 成功
     */
    SUCCESS(200),

    /**
     * 参数问题
     */
    FAIL(400),
    /**
     * 参数验证失败
     */
    PARAMETER_VALIDATION(3001),
    /**
     * 参数校验失败
     */
    PARAMETER_VERIFY_FAILED(1005),
    /**
     * 其他错误
     */
    OTHER_ERRORS(433),
    /**
     * 认证失败
     */
    AUTHENTICATION_FAILED(401),
    /**
     * 执行授权失败
     */
    AUTHORITY_FAILED(406),
    /**
     * 文件上传失败
     */
    FILE_UPLOAD_FAILED(416),
    /**
     * 重复操作
     */
    REPEAT_OPERATION(10031),

    /**
     * token不合法
     */
    TOKEN_INVALID(4007),
    /**
     * 数据未找到
     */
    DATA_NOT_FOUND(1000),
    /**
     * 缺失必须参数
     */
    PARAMETER_MISS(3000),
    /**
     * 数据状态错误
     */
    DATA_STATUS_ERROR(3005),

    /**
     * 系统异常
     */
    SYSTEM_ERROR(500);

    private Integer value;

    private Code(Integer value) {
        this.value = value;
    }

    public Integer value() {
        return this.value;
    }
}
