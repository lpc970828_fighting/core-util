package com.rt.model;

import com.rt.constants.Code;
import com.rt.constants.Message;
import lombok.Data;

@Data
public class Response<T> {

    /**
     * 响应码：详见{@link Code}
     */
    private Integer code;

    /**
     * 响应消息：详见{@link Message}
     */
    private String message;

    /**
     * 响应数据
     */
    private T data;

    public Response() {
        this.code = Code.SUCCESS.value();
        this.message = Message.SUCCESS.value();
    }

    public Response(T data) {
        this.data = data;
    }

    public Response(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Response(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> Response<T> success(T data) {
        return new Response<>(Code.SUCCESS.value(), Message.SUCCESS.value(), data);
    }

    public static <T> Response<T> error() {
        return new Response<>(Code.SYSTEM_ERROR.value(), Message.UNKNOWN_ERROR.value(), null);
    }

    public static <T> Response<T> error(String message) {
        return new Response<>(Code.SYSTEM_ERROR.value(), message, null);
    }

    public static <T> Response<T> fail(Code code, Message message) {
        return new Response<>(code.value(), message.value(), null);
    }

    public static <T> Response<T> fail(Integer code, String message) {
        return new Response<>(code, message, null);
    }

    public static <T> Response<T> fail(Integer code, Message message) {
        return new Response<>(code, message.value(), null);
    }

}
