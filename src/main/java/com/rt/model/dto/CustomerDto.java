package com.rt.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @ClassName CustomerDto
 * @Author Ray
 * @Date 2020/10/26 11:05
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerDto {

    @ApiModelProperty("用户登录名（必填）")
    @NotBlank(message = "name必填！")
    private String name;

    @ApiModelProperty("用户密码（必填）")
    @NotBlank(message = "password必填！")
    @Length(min = 1,max = 16,message = "密码长度为1-16！")
    private String password;


    @ApiModelProperty("盐")
    private String pwSalt;
}
