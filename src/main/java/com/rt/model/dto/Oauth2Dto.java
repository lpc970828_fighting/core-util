package com.rt.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

@Data
@ApiModel(value = "Oauth2Token")
public class Oauth2Dto {

    @NotBlank(message = "grantType必填！")
    @ApiModelProperty("类型:authorization_code/password/client_credentials（必填）")
    private String grantType;

    @NotBlank(message = "clientId必填！")
    @ApiModelProperty("第三方应用的标识ID（必填）")
    private String clientId;

    @ApiModelProperty("授权码模式的code（授权码模式必填）")
    private String code;

    @ApiModelProperty("用户登录名（密码模式必填）")
    private String username;

    @ApiModelProperty("用户密码（密码模式必填）")
    @Length(min = 1, max = 20, message = "密码长度为1-20！")
    private String password;

    @NotBlank(message = "客户端密钥必填！")
    @ApiModelProperty("客户端密钥（必填）")
    private String clientSecret;

    @ApiModelProperty("标识授权范围（客户端/密码模式可选）")
    private String scope;

}

