package com.rt.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @Date: 2020/7/3 11:47
 * @Description: 刷新accessToken
 */
@Data
@ApiModel(value = "RefreshTokenDto")
public class RefreshTokenDto {

    @ApiModelProperty("refreshToken（必填）")
    @NotBlank(message = "refreshToken必填！")
    private String refreshToken;

    @ApiModelProperty("第三方应用的标识ID（必填）")
    @NotBlank(message = "clientId必填！")
    private String clientId;

    @ApiModelProperty("客户端密钥（必填）")
    @NotBlank(message = "clientSecret必填！")
    private String clientSecret;

    @ApiModelProperty("用户登录名（必填）")
    @NotBlank(message = "username必填！")
    private String username;

    @ApiModelProperty("用户密码（必填）")
    @NotBlank(message = "password必填！")
    @Length(min = 1,max = 16,message = "密码长度为1-16！")
    private String password;

    @ApiModelProperty("标识授权范围（可选）")
    private String scope;
}