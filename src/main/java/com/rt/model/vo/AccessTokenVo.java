package com.rt.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("accessToken")
public class AccessTokenVo {

    @ApiModelProperty("访问令牌")
    private String accessToken;

    @ApiModelProperty("过期时间")
    private Long expiresIn;

    @ApiModelProperty("刷新令牌")
    private String refreshToken;

    @ApiModelProperty("token_type")
    private String tokenType;

    @ApiModelProperty("授权范围")
    private String scope;

    public AccessTokenVo(String accessToken, Long expiresIn, String refreshToken, String tokenType, String scope) {
        this.accessToken = accessToken;
        this.expiresIn = expiresIn;
        this.refreshToken = refreshToken;
        this.tokenType = tokenType;
        this.scope = scope;
    }

    public AccessTokenVo(String accessToken, Long expiresIn, String tokenType, String scope) {
        this.accessToken = accessToken;
        this.expiresIn = expiresIn;
        this.tokenType = tokenType;
        this.scope = scope;
    }
}