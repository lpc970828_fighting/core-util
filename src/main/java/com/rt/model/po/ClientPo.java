package com.rt.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName ClientPo
 * @Author Ray
 * @Date 2020/11/23 14:13
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("client")
public class ClientPo {

    @TableId(type = IdType.AUTO)
    private Long id;
    @TableField("client_id")
    private String clientId;
    @TableField("client_secret")
    private String clientSecret;
}
