package com.rt.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName ClickPo
 * @Author Ray
 * @Date 2020/11/12 17:31
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("click")
public class ClickPo {

    @TableId(type = IdType.AUTO)
    private Long id;
    @TableField("sso_id")
    private String ssoId;
    @TableField("count")
    private String count;
}
