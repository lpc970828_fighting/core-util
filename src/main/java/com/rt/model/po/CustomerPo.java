package com.rt.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

/**
 * @author Ray
 * @version 1.0
 * @date 2020/9/29 16:53
 */
@Data
@TableName("customer")
@Builder
public class CustomerPo {

    @TableField("sso_id")
    private String ssoId;
    @TableField("customer_name")
    private String customerName;
    @TableField("customer_password")
    private String customerPassword;
    @TableField("customer_password_salt")
    private String customerPasswordSalt;
}
