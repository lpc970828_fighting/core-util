package com.rt.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

/**
 * @ClassName SequencePo
 * @Author Ray
 * @Date 2020/11/19 14:58
 * @Description
 */
@Data
@TableName("sequence")
@Builder
public class SequencePo {

    @TableField("seq_name")
    private String seqName;
    @TableField("current_val")
    private String currentVal;
    @TableField("increment_val")
    private String incrementVal;
}
