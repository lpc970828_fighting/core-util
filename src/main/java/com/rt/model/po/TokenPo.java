package com.rt.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@TableName("token")
@Builder
public class TokenPo {

    @TableField("id")
    private String id;

    /**
     * 用户sso_id, users_things表主键
     */
    @TableField("sso_id")
    private String ssoId;

    /**
     * app_info 表主键
     */
    @TableField("app_info_id")
    private Integer appInfoId;

    private String token;

    /**
     * token 类型, A - access_token R - refresh_token
     */
    private String type;

    /**
     * 更新时间
     */
    @TableField("update_dt")
    private LocalDateTime updateDt;

    /**
     * 创建者ssoid, users_things 表主键
     */
    @TableField("create_by")
    private String createBy;

}