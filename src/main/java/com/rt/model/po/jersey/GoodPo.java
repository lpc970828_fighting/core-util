package com.rt.model.po.jersey;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * <p/>
 *
 * @author Ray
 * @date: 2021.07.18 16:48
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("good")
public class GoodPo {

    @TableId(type = IdType.AUTO)
    private Long id;
    @TableField("name")
    private String name;
    @TableField("count")
    private String count;
}
