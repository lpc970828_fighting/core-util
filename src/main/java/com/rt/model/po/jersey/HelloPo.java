package com.rt.model.po.jersey;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * <p/>
 *
 * @author Ray
 * @date: 2021.07.18 15:31
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("hello")
public class HelloPo {

    @TableId(type = IdType.AUTO)
    private Long id;
    @TableField("name")
    private String name;
    @TableField("count")
    private String count;
}
