package com.rt.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author：Ray
 * @ProjectName：Information
 * @Description：用户实体
 * @date：11:11 2020/3/4
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 用户登录名
     */
    private String userName;

    /**
     * 用户密码
     */
    private String userPassword;

    /**
     * 用户联系方式
     */
    private String userMobile;

    /**
     * 用户邮箱
     */
    private String userEmail;

    /**
     * 用户角色 0-超管 1-游客 2-用户
     */
    private Integer role;

}
