package com.rt.exceptions;

import com.rt.constants.Code;
import com.rt.constants.Message;
import com.rt.exception.CustomException;

/**
 * 数据重复异常
 *
 * @author Ray
 */
public class RepeatOperationException extends CustomException {

    public RepeatOperationException(Integer errorCode) {
        super(errorCode, Message.REPEAT_OPERATION.value());
        setErrorCode(Code.REPEAT_OPERATION.value());
    }

    public RepeatOperationException(String errorMessage) {
        super(errorMessage);
        setErrorCode(Code.REPEAT_OPERATION.value());
    }

    public RepeatOperationException(Integer errorCode, String errorMessage) {
        super(errorCode, errorMessage);
        setErrorCode(Code.REPEAT_OPERATION.value());
    }
}