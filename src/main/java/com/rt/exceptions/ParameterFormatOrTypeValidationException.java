package com.rt.exceptions;

import com.rt.constants.Code;
import com.rt.constants.Message;
import com.rt.exception.CustomException;

/**
 * 参数格式或类型验证异常
 *
 * @author Ray
 */
public class ParameterFormatOrTypeValidationException extends CustomException {

    /**
     *
     */
    private static final long serialVersionUID = 2543368572595498027L;

    public ParameterFormatOrTypeValidationException(Integer errorCode) {
        super(errorCode, Message.PARAMS_VALIDATION_FAILED.value());
        setErrorCode(Code.PARAMETER_VALIDATION.value());
    }

    public ParameterFormatOrTypeValidationException(String errorMessage) {
        super(errorMessage);
        setErrorCode(Code.PARAMETER_VALIDATION.value());
    }

    public ParameterFormatOrTypeValidationException(Integer ErrorCode, String errorMessage) {
        super(ErrorCode, errorMessage);
        setErrorCode(Code.PARAMETER_VALIDATION.value());
    }
}
