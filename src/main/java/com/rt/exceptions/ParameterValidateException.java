package com.rt.exceptions;


import com.rt.constants.Code;
import com.rt.constants.Message;

/**
 * @since 2020/6/20
 */
public class ParameterValidateException extends RuntimeException {

    private static final long serialVersionUID = -4263017933060179278L;
    private Integer code;

    public ParameterValidateException() {
        super(Message.PARAMETER_VALIDATION.value());
        this.code = Code.PARAMETER_VALIDATION.value();
    }

    public ParameterValidateException(Message message) {
        super(message.value());
        this.code = Code.PARAMETER_VALIDATION.value();
    }

    public ParameterValidateException(String message) {
        super(message);
        this.code = Code.PARAMETER_VALIDATION.value();
    }

    public Integer code() {
        return this.code;
    }

    @Override
    public String toString() {
        return "UnauthorizedException{" +
                "code=" + code + ", message=" + this.getMessage() +
                '}';
    }
}
