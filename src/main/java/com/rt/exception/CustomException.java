package com.rt.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author：Ray
 * @ProjectName：Information
 * @Description：自定义异常类
 * @date：15:47 2020/3/4
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class CustomException extends RuntimeException {


    private static final long serialVersionUID = -2013099994778186749L;

    private Integer errorCode;
    private String errorMessage;

    @Override
    public String getMessage() {
        return errorMessage;
    }

    public CustomException(Integer errorCode, String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    public CustomException(Integer errorCode) {
        super();
        this.errorCode = errorCode;
    }

    public CustomException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }


}
